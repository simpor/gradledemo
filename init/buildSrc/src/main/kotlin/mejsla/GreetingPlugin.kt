package mejsla

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.FileCollection
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.Copy
import org.gradle.kotlin.dsl.invoke
import org.gradle.kotlin.dsl.register

open class GreetingPluginExtension {
    lateinit var name: String
}

class GreetingPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        val extension = project.extensions.create("greeting", GreetingPluginExtension::class.java)
        project.run {
            tasks {
                register("greetingPlug") {
                    group = "Mejsla"
                    description = "say hello..."
                    println("--- gretting plugin ---")
                    println("--- gretting plugin - ${extension.name} ---")
                    doLast {
                        println("hello ${extension.name}!!!")
                    }
                }
            }
        }
    }
}
