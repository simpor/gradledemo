plugins {
    `kotlin-dsl`
    `java-gradle-plugin`
}

repositories {
    jcenter()
}

//https://quickbirdstudios.com/blog/gradle-kotlin-buildsrc-plugin-android/
dependencies {

//    included by the "java-gradle-plugin"
//    implementation(gradleApi())
//    implementation(localGroovy())

}

gradlePlugin {
    plugins {
        create("greetingPlugin") {
            id = "mejsla.greeting"
            implementationClass = "mejsla.GreetingPlugin"
            description = "The coolest plugin"
        }
    }
}




