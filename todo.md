# start
hello gradle med gradle init
start.spring.io
Kör gradle init
Skapa en java applikation
Visa filerna skapade
	gradle wrappern
	versionen i gradle wrappern
	settings.gradle
	build.gradle
Kör gradle tasks
Kör gradle build
	prata om bygg stegen, compile, build, test…
	--info
	--stacktrace
	--profile
	daemon    gradlew --status
	continues build   gradlew build -t
	inga test…. -x test
köra om tester -> test --rerun-tasks
	korta kommandon
	

# beroenden
implementation

api

constraints

```groovy
    constraints {
        implementation("com.google.guava:guava") {
            version {
                strictly("[10.0, 28[")
                prefer("28.1-jre")
                reject("27.0")
                because("""
            Uses APIs introduced in 10.0 but removed in 29. Tested with 28.1-jre.
            Known issues with 27.0
        """)
            }
        }
        implementation("org.junit.jupiter:junit-jupiter-api") {
            version {
                strictly("[5, 6[")  // någon version mellan 5 och 6
            }
        }
    }
```
		
#hantera beroenden
spring sättet
gradle sättet


#byggcykel
Initiering

Konfigurering

Exekvering

Gör exemplet med println i 
-- settings.gradle
-- build.gradle

skapa en task
	body println
	doFirst, doLast

#tasks
task("copyTestReport", type: Copy) {
tasks.register("copyTestReport", Copy) {
task copyTestReport(type: Copy) {

```groovy
tasks.register("copyConfig", Copy) {
    dependsOn = [build]
    description = "Kopierar configfilerna"
    group = "Mejsla"
    from "src/main/resources/"
    into "$buildDir/filteredConfig"

    filter { line ->
        line.replaceAll("@env@", "produktion")
    }
}

task copyTestReport(type: Copy) {
    dependsOn = [build, hello2]
    description = "Kopierar en junit test rapport till build katalogen"
    group = "Mejsla"
    from "$buildDir/test-results/test"
    include "*.xml"

    into "$buildDir"
}

// finalized build
tasks.build.finalizedBy("copyTestReport")


```

task register för att få upp prestanda
kan ersätta afterEvaluate

# timing av tasks
```groovy
gradle.taskGraph.beforeTask { task ->
    task.ext.setProperty("startTime", Instant.now())
}

gradle.taskGraph.afterTask { task, state ->
    println "Timing of task: " + task.name + " took " + Duration.between(task.ext.startTime, Instant.now()).toSeconds() + " seconds"
}
```

#multimodule
multi module api/implementation: https://docs.gradle.org/current/samples/sample_jvm_multi_project_build.html

Kör bygget
Flytta till en fil
Visa skillnaden mellan api och implementation

allProjects
subProjects



#test snippet
https://docs.gradle.org/current/dsl/org.gradle.api.tasks.testing.Test.html
```groovy
test {
    useJUnitPlatform()

    def failedTests = []
    def succeededTests = []

    afterTest { desc, result ->
        println("\n----------- TEST ${desc.className}.${desc.name}: ${result.resultType} -----------\n")
        def runningTime = result.endTime - result.startTime;
        if (result.failedTestCount != 0L) {
            failedTests.add("Test ($runningTime ms): ${desc.className}.${desc.name} -> ${result.exception?.message}")
        } else {
            succeededTests.add("SUCCESS ($runningTime ms): ${desc.className}.${desc.name}")
        }
    }
    afterSuite { desc, result ->
        if (desc.parent != null) {
            println("\n\n----------- TEST SUMMARY for ${desc.name} -----------")
            println("Results: ${result.resultType} (${result.testCount} tests, ${result.successfulTestCount} successes, ${result.failedTestCount} failures, ${result.skippedTestCount} skipped)")
            println("Successful tests: ")
            succeededTests.forEach { println(it) }
            println("\nFailed tests: ")
            failedTests.forEach { println(it) }
            println("\n------------------------------------\n\n")
        }
        if (properties.containsKey("keepAlive")) {
            while (true) {
                println("Sleeping to avoid shutdown of test, press ctrl+C to shutdown")
                Thread.sleep(5000)
            }
        }

    }
}
```

# multimodule
multi module api/implementation: https://docs.gradle.org/current/samples/sample_jvm_multi_project_build.html

Gå igenom strukturen av bygget, rita på tavlan

Visa settings.gradle, och de olika byggfilerna
Visa vad som händer med api/implementation i utilitites

Merga till en byggfil

Visa allProjects
subprojects


# plugins
build.gradle.kts
```kotlin
plugins {
    `kotlin-dsl`
    `java-gradle-plugin`
}

repositories {
    jcenter()
}

//https://quickbirdstudios.com/blog/gradle-kotlin-buildsrc-plugin-android/
dependencies {

//    included by the "java-gradle-plugin"
//    implementation(gradleApi())
//    implementation(localGroovy())

}

gradlePlugin {
    plugins {
        create("greetingPlugin") {
            id = "mejsla.greeting"
            implementationClass = "mejsla.GreetingPlugin"
            description = "The coolest plugin"
        }
    }
}
```

GreetingPlugin.kt
```kotlin
package mejsla

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.FileCollection
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.Copy
import org.gradle.kotlin.dsl.invoke
import org.gradle.kotlin.dsl.register

open class GreetingPluginExtension {
    lateinit var name: String
}

class GreetingPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        val extension = project.extensions.create("greeting", GreetingPluginExtension::class.java)
        project.run {
            tasks {
                register("greetingPlug") {
                    group = "Mejsla"
                    description = "say hello..."
                    println("--- gretting plugin ---")
                    println("--- gretting plugin - ${extension.name} ---")
                    doLast {
                        println("hello ${extension.name}!!!")
                    }
                }
            }
        }
    }
}

```